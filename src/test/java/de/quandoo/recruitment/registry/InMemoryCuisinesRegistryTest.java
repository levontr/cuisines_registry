package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class InMemoryCuisinesRegistryTest {

    private static final String FRENCH = "FRENCH";
    private static final String GERMAN = "GERMAN";
    private static final String ITALIAN = "ITALIAN";

    @Test
    public void shouldReturnSingleFrenchCuisine() {
        //given
        InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();
        cuisinesRegistry.register(new Customer("1"), new Cuisine(FRENCH));
        cuisinesRegistry.register(new Customer("2"), new Cuisine(GERMAN));
        cuisinesRegistry.register(new Customer("3"), new Cuisine(ITALIAN));

        //when
        List<Customer> frenchCustomers = cuisinesRegistry.cuisineCustomers(new Cuisine(FRENCH));

        //then
        assertEquals(1, frenchCustomers.size());
        assertEquals("1", frenchCustomers.get(0).getName());
    }

    @Test
    public void shouldReturnTwoGermanCuisine() {

        //given
        InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();
        cuisinesRegistry.register(new Customer("1"), new Cuisine(FRENCH));
        cuisinesRegistry.register(new Customer("2"), new Cuisine(GERMAN));
        cuisinesRegistry.register(new Customer("3"), new Cuisine(GERMAN));
        cuisinesRegistry.register(new Customer("4"), new Cuisine(ITALIAN));

        //when
        List<Customer> germanCustomers = cuisinesRegistry.cuisineCustomers(new Cuisine(GERMAN));

        //then
        assertEquals(2, germanCustomers.size());
        assertTrue(germanCustomers.contains(new Customer("2")));
        assertTrue(germanCustomers.contains(new Customer("3")));
    }

    @Test
    public void shouldReturn() {

        //given
        InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();
        cuisinesRegistry.register(new Customer("1"), new Cuisine(FRENCH));
        cuisinesRegistry.register(new Customer("2"), new Cuisine(GERMAN));
        cuisinesRegistry.register(new Customer("3"), new Cuisine(GERMAN));
        cuisinesRegistry.register(new Customer("2"), new Cuisine(ITALIAN));

        //when
        List<Cuisine> cuisines = cuisinesRegistry.customerCuisines(new Customer("2"));

        //then
        assertEquals(2, cuisines.size());
        assertTrue(cuisines.contains(new Cuisine(GERMAN)));
        assertTrue(cuisines.contains(new Cuisine(ITALIAN)));
    }


    @Test
    public void shouldReturnEmptyListIfNoCuisine() {

        //given
        InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();
        cuisinesRegistry.register(new Customer("1"), new Cuisine(FRENCH));

        //when
        List<Customer> germanCustomers = cuisinesRegistry.cuisineCustomers(new Cuisine(GERMAN));

        //then
        assertTrue(germanCustomers.isEmpty());
    }

    @Test
    public void shouldReturnTopCuisineS() {

        //given
        InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();
        cuisinesRegistry.register(new Customer("1"), new Cuisine(FRENCH));
        cuisinesRegistry.register(new Customer("2"), new Cuisine(FRENCH));
        cuisinesRegistry.register(new Customer("3"), new Cuisine(GERMAN));
        cuisinesRegistry.register(new Customer("4"), new Cuisine(GERMAN));
        cuisinesRegistry.register(new Customer("5"), new Cuisine(GERMAN));
        cuisinesRegistry.register(new Customer("2"), new Cuisine(GERMAN));
        cuisinesRegistry.register(new Customer("1"), new Cuisine(GERMAN));
        cuisinesRegistry.register(new Customer("7"), new Cuisine(ITALIAN));
        cuisinesRegistry.register(new Customer("8"), new Cuisine(ITALIAN));
        cuisinesRegistry.register(new Customer("9"), new Cuisine(ITALIAN));
        cuisinesRegistry.register(new Customer("10"), new Cuisine("USA"));

        //when
        List<Cuisine> cuisines = cuisinesRegistry.topCuisines(3);

        //then
        assertEquals(3, cuisines.size());
        assertEquals(GERMAN, cuisines.get(0).getType());
        assertEquals(ITALIAN, cuisines.get(1).getType());
        assertEquals(FRENCH, cuisines.get(2).getType());

    }
}