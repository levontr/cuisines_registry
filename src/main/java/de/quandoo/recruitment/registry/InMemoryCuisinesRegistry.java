package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.Comparator.comparingInt;

/**
 * {@inheritDoc}
 *
 * In Memory Implementation of the Registry for Cuisines and Customers.
 */
public class InMemoryCuisinesRegistry implements CuisinesRegistry {

    private Map<Cuisine, List<Customer>> cuisineToCustomers = new HashMap<>();
    private Map<Customer, List<Cuisine>> customerToCuisine = new HashMap<>();

    /**
     * Registers cusomer and cuisine.
     *
     * @param customer
     * @param cuisine
     */
    @Override
    public void register(final Customer customer, final Cuisine cuisine) {
        cuisineToCustomers.computeIfAbsent(cuisine, c -> new LinkedList<>()).add(customer);
        customerToCuisine.computeIfAbsent(customer, c -> new LinkedList<>()).add(cuisine);
    }

    /**
     * Returns List of Customers for a given Cuisine.
     *
     * @param cuisine
     * @return
     */
    @Override
    public List<Customer> cuisineCustomers(final Cuisine cuisine) {
        return cuisineToCustomers.getOrDefault(cuisine, new LinkedList<>());
    }

    /**
     * Returns List of Cuisines for a given Customer.
     *
     * @param customer
     * @return
     */
    @Override
    public List<Cuisine> customerCuisines(final Customer customer) {
        return customerToCuisine.getOrDefault(customer, new LinkedList<>());
    }

    /**
     * Returns top n Cuisines with most registered Customers
     *
     * @param n Number of top cusines
     * @return top Cuisines
     */
    @Override
    public List<Cuisine> topCuisines(final int n) {
        return cuisineToCustomers.entrySet()
                .stream()
                .sorted(comparingInt((Map.Entry<Cuisine, List<Customer>> e) -> e.getValue().size()).reversed())
                .limit(n)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

}
