package de.quandoo.recruitment.registry.model;

import com.google.common.base.Preconditions;

import java.util.Objects;

public class Customer {

    private final String name;

    public Customer(final String name) {
        Preconditions.checkArgument(name!=null);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return Objects.equals(getName(), customer.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName());
    }

    @Override
    public String toString() {
        return "Customer{" +
                "name='" + name + '\'' +
                '}';
    }
}
