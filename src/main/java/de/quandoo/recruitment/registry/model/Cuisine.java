package de.quandoo.recruitment.registry.model;

import java.util.Objects;


public class Cuisine {

    private final String type;

    public Cuisine(final String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cuisine cuisine = (Cuisine) o;
        return Objects.equals(getType(), cuisine.getType());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getType());
    }

    @Override
    public String toString() {
        return "Cuisine{" +
                "type='" + type + '\'' +
                '}';
    }
}
